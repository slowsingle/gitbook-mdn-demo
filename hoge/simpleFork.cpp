#include <iostream>
#include <unistd.h>

void test();

int main(){

    int count = 0;
    count++;
    std::cout << "Count = ";
    std::cout << count << std::endl;

    pid_t pid;
    // 子プロセス作成
    pid = fork();

    // pidが0の場合は子プロセス
    // pidが-1の場合はforkに失敗
    // pidが0,-1以外の場合は親プロセス
    if( pid < 0 ) {
        std::cout << "Fork Failed." << std::endl;
        return 0;
    } else if( pid == 0 ) {
        putchar( '\n' );
        std::cout << "Child Process. pid = ";
        std::cout << getpid() << std::endl;
        std::cout << "My Parent pid = ";
        std::cout << getppid() << std::endl;
        count++;
    } else { 
        putchar( '\n' );
        std::cout << "Parent Process. pid = ";
        std::cout << getpid() << std::endl;
        test();
        std::cout << "My Child pid = ";
        std::cout << pid << std::endl;
        count++;
    }
    
    std::cout << "Count = ";
    std::cout << count << std::endl;

    putchar( '\n' );
    return 0;
}

void test(){
    pid_t pid;
    // 子プロセス作成
    pid = fork();

    printf("aa\n");

}