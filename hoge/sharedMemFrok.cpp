#include <iostream>
#include <sys/wait.h>
#include <sys/shm.h>
#include <unistd.h>

int main(){

    int count = 0;
    int status;
    int shmid; 
    int *shmaddr;
    std::cout << "Count = ";
    std::cout << count << std::endl;

    pid_t pid;
    // 子プロセス作成
    pid = fork();

    // 共有メモリセグメント作成
    if ((shmid = shmget(IPC_PRIVATE, 100, 0600)) == -1){
        perror( "shmget error." );
        exit( EXIT_FAILURE );
    }

    // pidが0の場合は子プロセス
    // pidが-1の場合はforkに失敗
    // pidが0,-1以外の場合は親プロセス
    if( pid < 0 ) {
        std::cout << "Fork Failed." << std::endl;
        return -1;
    } else if( pid == 0 ) {
        putchar( '\n' );
        std::cout << "Child Process. pid = ";
        std::cout << getpid() << std::endl;
        std::cout << "My Parent pid = ";
        std::cout << getppid() << std::endl;
        count++;

        //共有メモリattach
        if ((shmaddr = (int *)shmat(shmid, NULL, 0)) == (void *)-1) {
            perror( "ChildProcess shmat error." );
            exit(EXIT_FAILURE);
        }
        
        *shmaddr = count;

        //共有メモリのdetach
        if (shmdt(shmaddr) == -1) {
            perror( "ChildProcess shmdt error." );
            exit(EXIT_FAILURE);
        }

        exit(EXIT_SUCCESS);

    } else { 
        putchar( '\n' );
        std::cout << "Parent Process. pid = ";
        std::cout << getpid() << std::endl;
        std::cout << "My Child pid = ";
        std::cout << pid << std::endl;

        //Child Processが終わるのを待つ
        waitpid( pid, &status, 0 );
        std::cout << "Finishied. pid = ";
        std::cout << pid << std::endl;


        /* 共有メモリ・セグメントをプロセスのアドレス空間に付加 */
        /*
        if ((shmaddr = (int *)shmat(shmid, NULL, 0)) == (void *)-1) {
            perror( "ParentProcess shmat error." );
            exit(EXIT_FAILURE);
        }
        */

        std::cout << "Attache Count = ";
        std::cout << *shmaddr << std::endl;
         

        if (WIFEXITED(status)){
            printf("exit, status=%d\n", WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)){
            printf("signal, sig=%d\n", WTERMSIG(status));
        } else {
            printf("abnormal exit\n");
        }
        count++;
    }
    
    std::cout << "Count = ";
    std::cout << count << std::endl;

    putchar( '\n' );
    return 0;
}